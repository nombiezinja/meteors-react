# MeteorJS basic demo for LoginRadius JavaScript library 
![Home Image](http://docs.lrcontent.com/resources/github/banner-1544x500.png)

## Introduction ##
LoginRadius is an Identity Management Platform that simplifies user registration while securing data. LoginRadius Platform simplifies and secures your user registration process, increases conversion with Social Login that combines 30 major social platforms, and offers a full solution with Traditional Customer Registration. You can gather a wealth of user profile data from Social Login or Traditional Customer Registration.

LoginRadius centralizes it all in one place, making it easy to manage and access. Easily integrate LoginRadius with all of your third-party applications, like MailChimp, Google Analytics, Livefyre and many more, making it easy to utilize the data you are capturing.

LoginRadius helps businesses boost user engagement on their web/mobile platform, manage online identities, utilize social media for marketing, capture accurate consumer data, and get unique social insight into their customer base.

Please visit [here](http://www.loginradius.com/) for more information.

## Documentation
For full documentation visit [here](https://docs.loginradius.com/api/v2/deployment/demos/meteor-demo)

## Author

[LoginRadius](https://www.loginradius.com/)

## License

This project is licensed under the MIT license. See the [LICENSE](LICENSE) file for more info.


## Installing

* Clone repository, cd into directory and run the following commands in sequence: 
* `meteor npm install`
* `cp settings.json.sample settngs.json`
* Open the newly created `settings.json` and alter the values with your own 
* Run `meteor --settings settings.json` and visit `localhost:3000` to see the demo in action 